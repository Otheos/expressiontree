﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ExpressionTree
{
    public static class Extensions
    {
        public static Expression OrHasKitchen(this Expression input, bool? hasValue, ParameterExpression pe)
        {
            //NEED TO BUILD THE FOLLOWING EXPRESSION house.Rooms.Any(room => room.RoomType.Description == "Kitchen") == hasvalue

            if (hasValue == null) return input;

            //Create expression tree that represents the expression 'room.RoomType.Description == "Kitchen"'
            var rp = Expression.Parameter(typeof(Room), "room");
            var roomLeft = CreateExpression("RoomType.Description", rp);
            var roomRight = Expression.Constant("Kitchen", typeof(string));
            var roleTypeExpression = Expression.Equal(roomLeft, roomRight);

            //Create expression  house.Rooms.Any(room => room.RoomType.Description == "Kitchen") == hasvalue
            Expression left = BuildAny(Expression.Lambda<Func<Room, bool>>(roleTypeExpression, new ParameterExpression[] { rp }), pe);
            Expression right = Expression.Constant(hasValue, typeof(bool));
            Expression e = Expression.Equal(left, right);

            return Expression.OrElse(e, input);
        }

        public static Expression OrHasBedRoom(this Expression input, bool? hasValue, ParameterExpression pe)
        {

            if (hasValue == null) return input;

            var rp = Expression.Parameter(typeof(Room), "room");
            var roomLeft = CreateExpression("RoomType.Description", rp);
            var roomRight = Expression.Constant("Bedroom", typeof(string));
            var roleTypeExpression = Expression.Equal(roomLeft, roomRight);

            Expression left = BuildAny(Expression.Lambda<Func<Room, bool>>(roleTypeExpression, new ParameterExpression[] { rp }), pe);
            Expression right = Expression.Constant(hasValue, typeof(bool));
            Expression e = Expression.Equal(left, right);

            return Expression.OrElse(e, input);
        }

        private static Expression CreateExpression(string propertyName, ParameterExpression param)
        {
            Expression body = param; 
            foreach (var member in propertyName.Split('.'))
            {
                body = Expression.PropertyOrField(body, member);
            }
            return body;
        }

        static Expression BuildAny(Expression<Func<Room, bool>> predicate, ParameterExpression param)
        {
            var queyableMethod = typeof (Queryable).GetMethods().First(m => m.Name == "AsQueryable").MakeGenericMethod(typeof(Room)); // <-- This was the WIN

            MethodInfo method = typeof(Queryable).GetMethods().Single(m => m.Name == "Any" && m.GetParameters().Length == 2).MakeGenericMethod(typeof(Room));
            var call = Expression.Call(
                method,
                Expression.Call(queyableMethod, Expression.PropertyOrField(param, "Rooms")),
                predicate);

            return call;
        }
    }
}
