﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ExpressionTree
{
    class Program
    {
        static void Main(string[] args)
        {
            HasKitchen();
            NoKitchen();
            OnlyBedroom();
            BedRoomORKitchen();

            Console.ReadKey();
        }

        private static void HasKitchen()
        {
            var result = FilterHouses(new HouseFilter { HasKitchen = true });

            Console.WriteLine("Has Kitchen");
            Console.WriteLine("--------------");
            Console.WriteLine();
            Console.WriteLine("Expect: House1, House3");
            Console.WriteLine("Result: {0}", string.Join(", ", result.Select(h => h.Name)));
            Console.WriteLine();
            Console.WriteLine();
        }

        private static void NoKitchen()
        {
            var result = FilterHouses(new HouseFilter { HasKitchen = false });

            Console.WriteLine("No Kitchen");
            Console.WriteLine("--------------");
            Console.WriteLine();
            Console.WriteLine("Expect: House2");
            Console.WriteLine("Result: {0}", string.Join(", ", result.Select(h => h.Name)));
            Console.WriteLine();
            Console.WriteLine();
        }

        private static void OnlyBedroom()
        {
            var result = FilterHouses(new HouseFilter { HasBedRoom = true });

            Console.WriteLine("Has Bedroom");
            Console.WriteLine("--------------");
            Console.WriteLine();
            Console.WriteLine("Expect: House2, House3");
            Console.WriteLine("Result: {0}", string.Join(", ", result.Select(h => h.Name)));
            Console.WriteLine();
            Console.WriteLine();
        }

        private static void BedRoomORKitchen()
        {
            var result = FilterHouses(new HouseFilter { HasKitchen = true, HasBedRoom = true});

            Console.WriteLine("Has Bedroom OR Kitchen");
            Console.WriteLine("--------------");
            Console.WriteLine();
            Console.WriteLine("Expect: House1, House2, House3");
            Console.WriteLine("Result: {0}", string.Join(", ", result.Select(h => h.Name)));
            Console.WriteLine();
            Console.WriteLine();
        }

        public static IQueryable<House> FilterHouses(HouseFilter filters)
        {
            var houses = CreateHouses().AsQueryable();

            var pe = Expression.Parameter(typeof(House), "house");

            //Create base expression needed to start the chain
            var left = Expression.Constant(false, typeof(bool));
            var right = Expression.Constant(true, typeof(bool));
            Expression baseExpression = Expression.Equal(left, right);

            var filterExpression = baseExpression.OrHasKitchen(filters.HasKitchen, pe)
                                                 .OrHasBedRoom(filters.HasBedRoom, pe);

            if (filterExpression == baseExpression) return houses; 
            var filteredResults = houses.Where(Expression.Lambda<Func<House, bool>>(filterExpression, new ParameterExpression[] { pe }));

            return filteredResults;
        }

        /// <summary>
        /// Creates Data to use
        /// </summary>
        /// <returns></returns>
        public static List<House> CreateHouses()
        {
            var houses = new List<House>();

            var house1 = new House
            {
                Name = "House1",
                Rooms = new List<Room>
                {
                    new Room
                    {
                        Name = "House1Kitchen",
                        RoomType = new RoomType
                        {
                            Code = 1,
                            Description = "Kitchen"
                        }
                    },
                    new Room
                    {
                        Name = "House1Lounge",
                        RoomType = new RoomType
                        {
                            Code = 1,
                            Description = "Lounge"
                        }
                    }
                }
            };

            var house2 = new House
            {
                Name = "House2",
                Rooms = new List<Room>
                {
                    new Room
                    {
                        Name = "House2BedRoom",
                        RoomType = new RoomType
                        {
                            Code = 1,
                            Description = "Bedroom"
                        }
                    },
                    new Room
                    {
                        Name = "House2Lounge",
                        RoomType = new RoomType
                        {
                            Code = 1,
                            Description = "Lounge"
                        }
                    }
                }
            };

            var house3 = new House
            {
                Name = "House3",
                Rooms = new List<Room>
                {
                    new Room
                    {
                        Name = "House3BedRoom",
                        RoomType = new RoomType
                        {
                            Code = 1,
                            Description = "Bedroom"
                        }
                    },
                    new Room
                    {
                        Name = "House3Kitchen",
                        RoomType = new RoomType
                        {
                            Code = 1,
                            Description = "Kitchen"
                        }
                    }
                }
            };

            houses.Add(house1);
            houses.Add(house2);
            houses.Add(house3);

            return houses;
        }  
    }

    public class House
    {
        public List<Room> Rooms { get; set; }
        public string Name { get; set; }
        
    }

    public class Room
    {
        public RoomType RoomType { get; set; }
        public string Name { get; set; }
    }

    public class RoomType
    {
        public string Description { get; set; }
        public int Code { get; set; }
    }

    public class HouseFilter
    {
        public bool? HasKitchen { get; set; }
        public bool? HasBedRoom { get; set; }
    }
}
